package main

import (
	"log"
	"os"
	"strconv"
	"strings"
)

func loadInput() []string {
	content, err := os.ReadFile("in.txt")
	if err != nil {
		log.Fatal(err)
	}
	newStrings := strings.Split(string(content), "\n")
	return newStrings
}

func stringArrToIntArr(arr []string) []int {
	out := make([]int, len(arr))

	for i, val := range arr {
		newint, err := strconv.Atoi(val)
		if err != nil {
			panic(err)
		}
		out[i] = newint
	}

	return out
}

func checkRangeEncapsulates(val int, second []int) bool {
	if val >= second[0] && val <= second[1] {
		return true
	}
	return false
}

func processRow(row string) int {
	shifts := strings.Split(row, ",")
	var shiftOneVals = stringArrToIntArr(strings.Split(shifts[0], "-"))
	var shiftTwoVals = stringArrToIntArr(strings.Split(shifts[1], "-"))

	var firstInRange = checkRangeEncapsulates(shiftOneVals[0], shiftTwoVals) || checkRangeEncapsulates(shiftOneVals[1], shiftTwoVals)
	var secondInRange = checkRangeEncapsulates(shiftTwoVals[0], shiftOneVals) || checkRangeEncapsulates(shiftTwoVals[1], shiftOneVals)

	log.Println(shiftOneVals, shiftTwoVals)
	if firstInRange || secondInRange {
		return 1
	}

	return 0
}

func main() {
	data := loadInput()
	var total = 0
	for _, row := range data {
		if len(row) == 0 {
			continue
		}
		var res = processRow(row)
		log.Println(res)
		total = total + res
	}

	log.Printf("Total is %d\n", total)
}
