package main

import (
	"fmt"
	"log"
	"os"
	//"sort"
	//"strconv"
	"strings"
)

const DRAW_SCORE = 3
const WIN_SCORE = 6
const LOSS_SCORE = 0

func loadInputt() []string {
	content, err := os.ReadFile("in.txt")
	if err != nil {
		log.Fatal(err)
	}
	newStrings := strings.Split(string(content), "\n")
	return newStrings
}

// Rock A
// Paper B
// Scissor C

// Rock X
// Paper Y
// Scissor Z
func calcScore(opponentMove, playerMove string) int {
	winCondition := map[string]string{
		"A": "Y",
		"B": "Z",
		"C": "X",
		"X": "B",
		"Y": "C",
		"Z": "A",
	}
	actionScore := map[string]int{
		"X": 1,
		"Y": 2,
		"Z": 3,
	}

	if winCondition[opponentMove] == playerMove {
		fmt.Printf("Player WIN  with %s against %s \n", opponentMove, playerMove)
		return WIN_SCORE + actionScore[playerMove]
	}

	if opponentMove == winCondition[playerMove] {
		fmt.Printf("Player LOSE with %s against %s \n", opponentMove, playerMove)
		return LOSS_SCORE + actionScore[playerMove]
	}

	fmt.Printf("Player DRAW with %s against %s \n", opponentMove, playerMove)
	return DRAW_SCORE + actionScore[playerMove]
}

func main() {
	matches := loadInput()
	var total = 0
	for _, val := range matches {
		moves := strings.Split(val, " ")
		if len(moves) != 2 {
			continue
		}
		total = total + calcScore(moves[0], moves[1])
	}
	fmt.Printf("Total score: %d \n", total)
}
