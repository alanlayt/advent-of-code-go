package main

import (
	"log"
	"os"
	"strconv"
	"strings"
)

func loadInput() []string {
	content, err := os.ReadFile("in.txt")
	if err != nil {
		log.Fatal(err)
	}
	newStrings := strings.Split(string(content), "\n")
	return newStrings
}

func stringArrToIntArr(arr []string) []int {
	out := make([]int, len(arr))

	for i, val := range arr {
		newint, err := strconv.Atoi(val)
		if err != nil {
			panic(err)
		}
		out[i] = newint
	}

	return out
}

func checkRangeEncapsulates(first, second []int) int {
	log.Println(first, second)
	if first[0] >= second[0] && first[1] <= second[1] {
		return 1
	}
	return 0
}

func processRow(row string) int {
	shifts := strings.Split(row, ",")
	var shiftOneVals = stringArrToIntArr(strings.Split(shifts[0], "-"))
	var shiftTwoVals = stringArrToIntArr(strings.Split(shifts[1], "-"))

	if checkRangeEncapsulates(shiftOneVals, shiftTwoVals) > 0 || checkRangeEncapsulates(shiftTwoVals, shiftOneVals) > 0 {
		return 1
	}

	return 0
}

func main() {
	data := loadInput()
	var total = 0
	for _, row := range data {
		if len(row) == 0 {
			continue
		}
		var res = processRow(row)
		log.Println(res)
		total = total + res
	}

	log.Printf("Total is %d\n", total)
}
