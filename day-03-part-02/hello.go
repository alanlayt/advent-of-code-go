package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

const SCORE_MAP = "abcdefghijklmnopqrstuvwxyz"

func loadInput() []string {
	content, err := os.ReadFile("in.txt")
	if err != nil {
		log.Fatal(err)
	}
	newStrings := strings.Split(string(content), "\n")
	return newStrings
}

func calcScore(char string) int {
	if strings.Index(SCORE_MAP, char) > -1 {
		return strings.Index(SCORE_MAP, char) + 1
	}

	return strings.Index(strings.ToUpper(SCORE_MAP), char) + len(SCORE_MAP) + 1
}

func condenseOverlaps(first, second string) string {
	var out string = ""

	for _, val := range strings.Split(second, "") {
		if strings.Contains(first, val) {
			out = out + val
		}
	}

	return out
}

func main() {
	var total = 0
	matches := loadInput()
	println(matches)

	for index, val := range matches {
		if len(val) == 0 {
			continue
		}

		if index%3 == 2 {
			var overlaps = condenseOverlaps(condenseOverlaps(matches[index-2], matches[index-1]), matches[index])
			if len(overlaps) == 0 {
				panic("No overlaps found")
			}
			var badge = overlaps[0:1]
			var score = calcScore(badge)
			total = total + score
			fmt.Printf("Badge %s has score %d \n", badge, score)
		}
	}

	fmt.Printf("Total score: %d \n", total)
}
